const Usuario = require('../models/usuario');

const validateUniqueEmail = async (email = '') => {
	const user = await Usuario.findOne({ email: email.toLowerCase() }).lean();

	if (user) {
		throw new Error(`El correo electrónico ${email} ya está registrado`);
	}
};

module.exports = {
	validateUniqueEmail
};
