const dbValidators = require('./db-validators');
const jwt = require('./jwt');

module.exports = {
	...dbValidators,
	...jwt
};
