const jwt = require('jsonwebtoken');

const generateJWT = (uid = '', email = '') => {
	const secret = process.env.JWT_SECRET;

	if (!secret) {
		throw new Error('No hay semilla de JWT - Revisar variables de entorno');
	}

	return new Promise((resolve, reject) => {
		try {
			const payload = { uid, email };

			jwt.sign(payload, secret, { expiresIn: '1d' }, (err, token) => {
				if (err) {
					console.log(err);
					reject(new Error('Error al generar el JWT'));
				}

				resolve(token);
			});
		} catch (error) {
			console.log(error);
			reject(new Error('Error al firmar el JWT'));
		}
	});
};

const comprobarJWT = (token = '') => {
	try {
		if (!token) {
			return [false, null];
		}

		const secret = process.env.JWT_SECRET;

		if (!secret) {
			return [false, null];
		}

		const { uid } = jwt.verify(token, secret);

		return [true, uid];
	} catch (error) {
		console.log(error);
		return [false, null];
	}
};

module.exports = {
	comprobarJWT,
	generateJWT
};
