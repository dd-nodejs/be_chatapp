const { request, response } = require('express');
const jwt = require('jsonwebtoken');
const Usuario = require('../models/usuario');

const errors = [
	{
		value: `Unauthorized`,
		msg: 'No está autorizado para acceder a este recurso',
		param: 'authorize',
		location: 'authorize'
	}
];

const validateJWT = async (req = request, res = response, next) => {
	const token = req.header('x-token');

	if (!token) {
		return res.status(401).json({ errors });
	}

	const secret = process.env.JWT_SECRET;

	if (!secret) {
		return res.status(500).json({ errors });
	}

	try {
		const { uid } = jwt.verify(token, secret);

		const user = await Usuario.findById(uid);
		req.authenticated_user = user;

		// Verify that user exist
		if (!user) {
			throw new Error('Unauthorized');
		}

		next();
	} catch (error) {
		console.log(error);
		res.status(401).json({ errors });
	}
};

module.exports = {
	validateJWT
};
