const { Router } = require('express');
const { check } = require('express-validator');

const { validateFields, validateJWT } = require('../middlewares');
const { crearUsuario, listarUsuarios } = require('../controllers');
const { validateUniqueEmail } = require('../helpers');

const router = Router();

router.get('/', validateJWT, listarUsuarios);

router.post(
	'/',
	[
		check('nombre', 'El nombre del usuario no es válido').not().isEmpty(),
		check('email', 'El correo electrónico no es válido').isEmail(),
		check('email').custom(validateUniqueEmail),
		check(
			'password',
			'La contraseña del usuario no es válida (min 6 carácteres)'
		)
			.not()
			.isEmpty()
			.isLength({ min: 6 }),
		validateFields
	],
	crearUsuario
);

module.exports = router;
