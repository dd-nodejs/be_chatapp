const { Router } = require('express');
const { check } = require('express-validator');

const { validateJWT, validateFields } = require('../middlewares');
const { obtenerChat } = require('../controllers');

const router = Router();

router.get(
	'/:de',
	[validateJWT, check('de', 'El id no es válido').isMongoId(), validateFields],
	obtenerChat
);

module.exports = router;
