const { Router } = require('express');
const { check } = require('express-validator');
const { handleLogin, renovarJWT } = require('../controllers');

const { validateFields, validateJWT } = require('../middlewares');

const router = Router();

router.post(
	'/login',
	[
		check('email', 'El correo electrónico no es válido').isEmail(),
		check(
			'password',
			'La contraseña del usuario no es válida (min 6 carácteres)'
		)
			.not()
			.isEmpty(),
		validateFields
	],
	handleLogin
);

router.get('/renew', validateJWT, renovarJWT);

module.exports = router;
