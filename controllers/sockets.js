const Usuario = require('../models/usuario');
const Mensaje = require('../models/mensaje');

const usuarioConectado = async (uid = '') => {
	const usuario = await Usuario.findById(uid);

	usuario.online = true;
	await usuario.save();

	return usuario;
};

const usuarioDesconectado = async (uid = '') => {
	const usuario = await Usuario.findById(uid);

	usuario.online = false;
	await usuario.save();

	return usuario;
};

const listarUsuariosOnline = async () => {
	try {
		const usuarios = await Usuario.find().sort('-online');

		return usuarios;
	} catch (error) {
		console.error(error);
		return [];
	}
};

const grabarMensaje = async (payload) => {
	try {
		const mensaje = new Mensaje(payload);
		await mensaje.save();

		return mensaje;
	} catch (error) {
		console.error(error);
		return null;
	}
};

module.exports = {
	listarUsuariosOnline,
	grabarMensaje,
	usuarioConectado,
	usuarioDesconectado
};
