const { request, response } = require('express');
const bcrypt = require('bcryptjs');

const Usuario = require('../models/usuario');
const { generateJWT } = require('../helpers');

const listarUsuarios = async (req = request, res = response) => {
	try {
		const usuarios = await Usuario.find().sort({ createdAt: 'ascending' });

		res.json({
			usuarios,
			success: true
		});
	} catch (error) {
		console.error(error);
		return res.status(400).json({
			errors: [
				{
					value: 'internal error',
					msg: 'Lo sentimos ocurrio un error en el servidor',
					param: 'server',
					location: 'server google verify'
				}
			],
			success: false
		});
	}
};

const crearUsuario = async (req = request, res = response) => {
	const { nombre, email, password } = req.body;

	try {
		const user = new Usuario({
			email: email.toLowerCase(),
			nombre,
			online: false,
			password: bcrypt.hashSync(password)
		});

		await user.save({ validateBeforeSave: true });

		// ? Generar JWT
		const jwt = await generateJWT(user.id, user.email);

		res.json({
			user,
			jwt,
			success: true
		});
	} catch (error) {
		console.error(error);
		return res.status(400).json({
			errors: [
				{
					value: 'internal error',
					msg: 'Lo sentimos ocurrio un error en el servidor',
					param: 'server',
					location: 'server google verify'
				}
			],
			success: false
		});
	}
};

module.exports = {
	crearUsuario,
	listarUsuarios
};
