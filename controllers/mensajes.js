const { request, response } = require('express');

const Mensaje = require('../models/mensaje');

const obtenerChat = async (req = request, res = response) => {
	try {
		const { id } = req.authenticated_user;
		const mensajesDe = req.params.de;

		const mensajes = await Mensaje.find({
			$or: [
				{ de: id, para: mensajesDe },
				{ de: mensajesDe, para: id }
			]
		})
			.sort({ createdAt: 'ascending' })
			.limit(30);

		res.json({
			mensajes,
			success: true
		});
	} catch (error) {
		console.error(error);
		return res.status(400).json({
			errors: [
				{
					value: 'internal error',
					msg: 'Lo sentimos ocurrio un error en el servidor',
					param: 'server',
					location: 'server google verify'
				}
			],
			success: false
		});
	}
};

module.exports = {
	obtenerChat
};
