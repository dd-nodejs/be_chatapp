const { request, response } = require('express');
const bcrypt = require('bcryptjs');

const Usuario = require('../models/usuario');
const { generateJWT } = require('../helpers');

const handleLogin = async (req = request, res = response) => {
	const { email, password } = req.body;

	try {
		// ? Verify is user exist
		const user = await Usuario.findOne({ email });

		if (!user) {
			res.status(401).json({
				success: false,
				errors: [
					{
						value: `Correo electrónico ${email}`,
						msg: 'No existen coincidencias con las credenciales enviadas',
						param: 'login',
						location: 'login'
					}
				]
			});
		}

		// ? Verify is user password match
		const validPassword = bcrypt.compareSync(password, user.password);
		if (!validPassword) {
			res.status(401).json({
				success: false,
				errors: [
					{
						value: `Correo electrónico ${email}`,
						msg: 'No existen coincidencias con las credenciales enviadas',
						param: 'login',
						location: 'login'
					}
				]
			});
		}

		// ? Generar JWT
		const jwt = await generateJWT(user.id, user.email);

		res.json({
			user,
			jwt,
			success: true
		});
	} catch (error) {
		console.log(error);
		return res.status(500).json({
			errors: [
				{
					value: 'internal error',
					msg: 'Lo sentimos ocurrio un error en el servidor',
					param: 'server',
					location: 'server'
				}
			],
			success: false
		});
	}
};

const renovarJWT = async (req = request, res = response) => {
	try {
		const user = req.authenticated_user;

		// ? Generar un nuevo jwt
		const jwt = await generateJWT(user.id, user.email);

		res.json({
			user,
			jwt,
			success: true
		});
	} catch (error) {
		console.log(error);
		return res.status(500).json({
			errors: [
				{
					value: 'internal error',
					msg: 'Lo sentimos ocurrio un error en el servidor',
					param: 'server',
					location: 'server'
				}
			],
			success: false
		});
	}
};

module.exports = {
	handleLogin,
	renovarJWT
};
