const authController = require('../controllers/auth');
const mensajeController = require('../controllers/mensajes');
const userController = require('../controllers/users');
const socketsController = require('./sockets');

module.exports = {
	...authController,
	...mensajeController,
	...userController,
	...socketsController
};
