# Chat-App

## Instrucciones

Instalar node_modules.

```
npm install
```

Copiar el archivo **.env.example** y renombrarlo como **.env**. Debes utilizar este archivo como una plantilla para declarar todas las variables de entorno requeridas.

- PORT: Se refiere al puerto donde la aplicación correra.

```
PORT=8080
```

- DB_CNN: Es la cadena de conexión a la base de datos me mongo.

```
DB_CNN=mongodb://localhost:27017/chatdb
```

- JWT_SECRET: Esta es una cadena de string que nos ayudara a firmar los JWT para la autenticación.

[**_Puedes generar uno en este enlace_**](https://generate-secret.vercel.app/32).

Arrancar el servidor.

```
npm run dev
```

> **_NOTA:_** Es necesario instalar [**_Nodemon_**](https://www.npmjs.com/package/nodemon). Esta es una aplicación para correr la aplicación de NodeJS y al detectar cambios reiniciar el servidor. Instalala de manera global en tu equipo.

```
npm install -g nodemon
```
