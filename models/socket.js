const {
	usuarioConectado,
	usuarioDesconectado,
	listarUsuariosOnline,
	grabarMensaje
} = require('../controllers');
const { comprobarJWT } = require('../helpers');

class Socket {
	constructor(io) {
		this.io = io;
		this.socketEvents();
	}

	socketEvents() {
		this.io.on('connection', async (socket) => {
			/* console.log('Cliente conectado', socket.id); */

			// ? VALIDAR EL JWT
			const [success, uid] = comprobarJWT(socket.handshake.query['x-token']);

			// ! Si el JWT no es válido, desconectar
			if (!success) {
				return socket.disconnect();
			}

			// ? Saber que usuario esta activo
			await usuarioConectado(uid);

			// ? Unir al usuario a una sala de socket.io
			socket.join(uid);

			// ? Emitir todos los usuarios conectados
			this.io.emit('lista-usuarios', await listarUsuariosOnline());

			// ? Escuchar cuando el cliente envia un mensaje
			socket.on('mensaje-personal', async (payload) => {
				const mensaje = await grabarMensaje(payload);
				this.io.to(payload.para).emit('mensaje-personal', mensaje);
				this.io.to(payload.de).emit('mensaje-personal', mensaje);
			});

			// ? Disconnect
			socket.on('disconnect', async () => {
				// ! Marcar que el usuario se desconecto en la DB
				await usuarioDesconectado(uid);

				this.io.emit('lista-usuarios', await listarUsuariosOnline());
			});

			// TODO: Emitir todos los usuarios conectados
		});
	}
}

module.exports = Socket;
