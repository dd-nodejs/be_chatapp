const cors = require('cors');
const express = require('express');
const http = require('http');
const path = require('path');
const socketio = require('socket.io');
const { dbConnection } = require('../database/config');

const Socket = require('./socket');

class Server {
	constructor() {
		// ? Servidor de Express
		this.app = express();
		this.port = process.env.PORT;

		// ? Conectar a DB
		dbConnection();

		// ? HTTP Server
		this.server = http.createServer(this.app);

		// ? Configuración del socket server
		this.io = socketio(this.server, {
			/* ...socket server configs */
		});
	}

	configurarSocket() {
		// eslint-disable-next-line no-new
		new Socket(this.io);
	}

	middlewares() {
		// ? Desplegar el directorio publico
		this.app.use(express.static(path.resolve(__dirname, '../public')));

		// ? Lectura y parseo del body
		this.app.use(express.json());

		// ? CORS
		this.app.use(cors());

		// ? Users routes
		this.app.use('/api/users', require('../routes/users'));

		// ? Auth routes
		this.app.use('/api/auth', require('../routes/auth'));

		// ? Mensajes routes
		this.app.use('/api/mensajes', require('../routes/mensajes'));
	}

	execute() {
		// ? Inicializar middlewares
		this.middlewares();

		// ? Inicializar sockets
		this.configurarSocket();

		// ? Inicializar el servidor de express
		this.server.listen(this.port, () => {
			console.log(`Servidor corriendo en puerto: ${this.port}`);
		});
	}
}

module.exports = Server;
