const { Schema, model, models } = require('mongoose');

const usuarioSchema = new Schema(
	{
		nombre: {
			type: String,
			required: [true, 'El nombre del usuario es obligatorio']
		},
		email: {
			type: String,
			required: [true, 'El correo electrónico es obligagorio'],
			unique: true
		},
		password: {
			type: String,
			required: [true, 'La contraseña es oblogatoria']
		},
		online: {
			type: Boolean,
			default: false
		}
	},
	{
		timestamps: true
	}
);

// Replace toJSON method to skip model fields (avoid using arrow function for maintaint right scope of 'this')
usuarioSchema.methods.toJSON = function () {
	const {
		__v,
		password,
		createdAt,
		updatedAt,
		_id: uid,
		...rest
	} = this.toObject();

	return {
		uid,
		...rest
	};
};

usuarioSchema.index({ name: 'text', email: 'text' });

const Usuario = models.Usuario || model('Usuario', usuarioSchema);

module.exports = Usuario;
