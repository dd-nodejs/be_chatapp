const { Schema, model, models } = require('mongoose');

const mensajeSchema = new Schema(
	{
		de: {
			type: Schema.Types.ObjectId,
			required: [true, 'El remitente del mensaje es obligatorio']
		},
		para: {
			type: Schema.Types.ObjectId,
			required: [true, 'El receptor del mensaje es obligatorio']
		},
		mensaje: {
			type: String,
			required: [true, 'El cuerpo del mensaje es obligatorio']
		}
	},
	{
		timestamps: true
	}
);

// Replace toJSON method to skip model fields (avoid using arrow function for maintaint right scope of 'this')
mensajeSchema.methods.toJSON = function () {
	const { __v, ...rest } = this.toObject();

	return {
		...rest
	};
};

const Mensaje = models.Mensaje || model('Mensaje', mensajeSchema);

module.exports = Mensaje;
